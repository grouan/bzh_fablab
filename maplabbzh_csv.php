<?PHP

/*	#MapLabBZH La carte des FabLabs & Tiers Lieux de dissémination des usages numériques en Bretagne
  =================================================================================================
    Auteur	Guillaume Rouan | @grouan
    Blog	https://grouan.fr
  -------------------------------------------------------------------------------------------------
    Titre	= maplabbzh_csv.php
    Sources = https://gitlab.com/grouan/maplabbzh
    Licence	= CC BY-SA pour les contenus / EUPL pour le code source
    UI Liste = CSS Framework Bulma + Font Awesome
    UI Cartographie = Mapbox + OpenStreetMap
    Datas GeoJSON = Guillaume Rouan
    Création : octobre 2015 | v.4 juillet 2022
  -------------------------------------------------------------------------------------------------
*/

	// Sources JSON : Github
	$json_source = file_get_contents('https://gitlab.com/grouan/maplabbzh/raw/master/map_bzh_fablab.geojson');

	//$json_data = json_decode($json_source); // Fichier brut
	$json_data = json_decode($json_source, true);  // Dans un tableau

	// export csv
	$export = 'id;nom;type;adresse;cp;ville;site_web;flux_rss;twitter;facebook;courriel;telephone;longitude;latitude'."\n";

	// Recherche de l'identifiant dans le fichier JSON
	$nb_references = count ($json_data['features']);

	for ($i=0; $i < $nb_references; $i++) {
		// --- id > infos Pôle
		$export .= $json_data['features'][$i]['properties']['id'].';'
			.$json_data['features'][$i]['properties']['name'].';'
			.$json_data['features'][$i]['properties']['structure'].';'
			.$json_data['features'][$i]['properties']['adresse'].';'
			.$json_data['features'][$i]['properties']['cp'].';'
			.$json_data['features'][$i]['properties']['ville'].';'
			.$json_data['features'][$i]['properties']['web'].';'
			.$json_data['features'][$i]['properties']['rss'].';';
		if ($json_data['features'][$i]['properties']['twitter'] != '') { $export .= 'https://twitter.com/'.$json_data['features'][$i]['properties']['twitter'].';'; } else { $export .= ';'; }
		if ($json_data['features'][$i]['properties']['facebook'] != '') { $export .= 'https://facebook.com/'.$json_data['features'][$i]['properties']['facebook'].';'; } else { $export .= ';'; }
		$export .= $json_data['features'][$i]['properties']['email'].';'
			.$json_data['features'][$i]['properties']['tel'].';'
			.$json_data['features'][$i]['geometry']['coordinates'][0].';' // longitude
			.$json_data['features'][$i]['geometry']['coordinates'][1]."\n"; // latitude

  	}


?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Carte des FabLabs & Tiers-Lieux de Bretagne #MapLabBZH - Guillaume Rouan - 2015 - CC BY-SA / EUPL</title>

	<meta name="viewport" content="width=device-width, initial-scale=1">

</head>

<body>

	<pre><?PHP echo $export; ?></pre>

</body>
</html>
