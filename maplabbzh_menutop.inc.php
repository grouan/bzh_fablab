<?PHP
/*	#MapLabBZH La carte des FabLabs & Tiers Lieux de dissémination des usages numériques en Bretagne
  =================================================================================================
    Auteur	Guillaume Rouan | @grouan
    Blog	https://grouan.fr
  -------------------------------------------------------------------------------------------------
    Titre	= maplabbzh_menutop.inc.php
    Sources = https://gitlab.com/grouan/maplabbzh
    Licence	= CC BY-SA pour les contenus / EUPL pour le code source
    UI Liste = CSS Framework Bulma + Font Awesome
    UI Cartographie = Mapbox + OpenStreetMap
    Datas GeoJSON = Guillaume Rouan
    Création : octobre 2015 | v.4 juillet 2022
  -------------------------------------------------------------------------------------------------
*/
?>

<style type="text/css">

/*	.nav-toggle
	  position: absolute
	  top: 0
	  left: 0
	  right: 0
	  text-align: right

	  +media-query-lap
	    display: none

	.nav-toggle
	  @extend %dis-inline-block
	  margin: $bsu 0
	  border-radius: 4px
	  background-color: #fafafa
	  border: 1px solid #f1f1f1
	  padding: $hsu
	  line-height: 1
	  font-size: 12px
	  font-weight: 600
	  text-decoration: none
	  text-transform: uppercase

	.nav-menu-close
	  @extend %dis-none
*/

	#site-header:target // dealing with open / close buttons here
	  .nav-menu
	    @extend %dis-none
	  .nav-menu-close
	    @extend %dis-inline-block

	</style>

  <script src="http://bulma.io/vendor/clipboard-1.7.1.min.js"></script>
	<script src="http://bulma.io/lib/main.js"></script>

<!-- Menu TOP -->
<div class="container is-dark">
  <nav class="navbar is-transparent" role="navigation" aria-label="main navigation" style="background-color:transparent;border-bottom:1px solid #404040;">

  <div class="navbar-brand">

    <span class="nav-item">
        <a class="button is-dark" style="background-color:transparent;" href="maplabbzh_liste" target="_self" title="Liste">
          <span class="font-bold">Liste #MapLabBZH</span>
        </a>
    </span>

    <a class="navbar-item is-hidden-desktop" href="https://grouan.fr/maplabbzh/" target="_self" title="accédez au dépôt GitLab">
      <span class="icon" style="color: #FFF;">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#FFF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M20 9v11a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2V9"/><path d="M9 22V12h6v10M2 10.6L12 2l10 8.6"/></svg>
      </span>
    </a>
    <a class="navbar-item is-hidden-desktop" href="https://gitlab.com/grouan/maplabbzh" target="_blank" title="accédez au dépôt GitLab">
      <span class="icon" style="color: #FFF;">
        <svg role="img" width="24" height="24" viewBox="0 0 24 24" fill="#FFF" stroke="none" xmlns="http://www.w3.org/2000/svg"><title>GitLab</title><path d="m23.6004 9.5927-.0337-.0862L20.3.9814a.851.851 0 0 0-.3362-.405.8748.8748 0 0 0-.9997.0539.8748.8748 0 0 0-.29.4399l-2.2055 6.748H7.5375l-2.2057-6.748a.8573.8573 0 0 0-.29-.4412.8748.8748 0 0 0-.9997-.0537.8585.8585 0 0 0-.3362.4049L.4332 9.5015l-.0325.0862a6.0657 6.0657 0 0 0 2.0119 7.0105l.0113.0087.03.0213 4.976 3.7264 2.462 1.8633 1.4995 1.1321a1.0085 1.0085 0 0 0 1.2197 0l1.4995-1.1321 2.4619-1.8633 5.006-3.7489.0125-.01a6.0682 6.0682 0 0 0 2.0094-7.003z"/></svg>
      </span>
    </a>

  </div><? // navbar-brand ?>

  <div id="navMenuTop" class="navbar-menu">
    <div class="navbar-start">

    <span class="nav-item">
      <a class="button is-dark" style="background-color:transparent;" href="https://grouan.fr/maplabbzh/maplabbzh_carte" target="_blank" title="afficher la carte (en plein écran)">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#FFF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><circle cx="12" cy="10" r="3"/><path d="M12 21.7C17.3 17 20 13 20 10a8 8 0 1 0-16 0c0 3 2.7 6.9 8 11.7z"/></svg>&nbsp;
        <span class="font-bold">Carte</span>
      </a>
    </span>
    <span class="nav-item">
      <a class="button is-dark" style="background-color:transparent;" href="https://gitlab.com/grouan/maplabbzh/wikis" target="_blank" title="consultez le wiki">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#FFF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M14 2H6a2 2 0 0 0-2 2v16c0 1.1.9 2 2 2h12a2 2 0 0 0 2-2V8l-6-6z"/><path d="M14 3v5h5M16 13H8M16 17H8M10 9H8"/></svg>&nbsp;
        <span class="font-bold">Doc</span>
      </a>
    </span>
    <span class="nav-item">
      <a class="button is-dark" style="background-color:transparent;" href="maplabbzh_csv.php" target="_blank" title="affichez en .CSV (pour tableurs)">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#FFF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M21.44 11.05l-9.19 9.19a6 6 0 0 1-8.49-8.49l9.19-9.19a4 4 0 0 1 5.66 5.66l-9.2 9.19a2 2 0 0 1-2.83-2.83l8.49-8.48"></path></svg>&nbsp;
        <span class="font-bold">CSV</span>
      </a>
    </span>
    <span class="nav-item">
      <a class="button is-dark" style="background-color:transparent;" href="maplabbzh_datalab.php" target="_self" title="visualisez les datas">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#FFF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M3 3v18h18"/><path d="M18.7 8l-5.1 5.2-2.8-2.7L7 14.3"/></svg>&nbsp;
        <span class="font-bold">DataLab</span>
      </a>
    </span>
    <span class="nav-item">
      <a class="button is-dark" style="background-color:transparent;" href="/maplabbzh/" target="_self" title="retour à l'accueil principal #MapLabBzh">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#FFF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M20 9v11a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2V9"/><path d="M9 22V12h6v10M2 10.6L12 2l10 8.6"/></svg>&nbsp;
        <span class="font-bold">Accueil</span>
      </a>
    </span>

    </div>

    <div class="navbar-end">

      <span class="nav-item">
        <a class="button is-warning" style="background-color:#fc6d26;" href="https://gitlab.com/grouan/maplabbzh" target="_blank" title="voir l'ensemble des sources sur GitLab">
          <svg role="img" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><title>GitLab</title><path d="m23.6004 9.5927-.0337-.0862L20.3.9814a.851.851 0 0 0-.3362-.405.8748.8748 0 0 0-.9997.0539.8748.8748 0 0 0-.29.4399l-2.2055 6.748H7.5375l-2.2057-6.748a.8573.8573 0 0 0-.29-.4412.8748.8748 0 0 0-.9997-.0537.8585.8585 0 0 0-.3362.4049L.4332 9.5015l-.0325.0862a6.0657 6.0657 0 0 0 2.0119 7.0105l.0113.0087.03.0213 4.976 3.7264 2.462 1.8633 1.4995 1.1321a1.0085 1.0085 0 0 0 1.2197 0l1.4995-1.1321 2.4619-1.8633 5.006-3.7489.0125-.01a6.0682 6.0682 0 0 0 2.0094-7.003z"/></svg>&nbsp;
          <span class="font-bold">Dépôt</span>
        </a>
      </span>
      <span class="nav-item">
        <a class="button is-warning" href="https://gitlab.com/grouan/maplabbzh/-/raw/master/map_bzh_fablab.geojson?inline=false" target="_blank" title="accédez aux sources">
          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><ellipse cx="12" cy="5" rx="9" ry="3"></ellipse><path d="M21 12c0 1.66-4 3-9 3s-9-1.34-9-3"></path><path d="M3 5v14c0 1.66 4 3 9 3s9-1.34 9-3V5"></path></svg>&nbsp;
          <span class="font-bold">GeoJSON</span>
        </a>
      </span>

    </div>

  </div>

</nav>

</div>
